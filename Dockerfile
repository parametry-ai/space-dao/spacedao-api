FROM python:3.10-slim-bullseye

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /code
RUN mkdir -p /code/alembic/versions

# TODO make it a template to be updated by build.sh script
COPY ./dist/spacedaoapi-0.8.0-py3-none-any.whl /code/spacedaoapi-0.8.0-py3-none-any.whl

RUN pip install --upgrade pip

RUN pip install --no-cache-dir --upgrade /code/spacedaoapi-0.8.0-py3-none-any.whl

RUN pip cache purge

COPY ./alembic/script.py.mako /code/alembic/script.py.mako
COPY ./alembic.ini /code/alembic.ini
COPY ./alembic/env.py /code/alembic/env.py

EXPOSE 8000/tcp

# CMD ["spacedao_api", "--workers", "2", "--host", "0.0.0.0", "--port", "8000"]
CMD ["/bin/bash",  "-c", "alembic revision --autogenerate -m 'Potential Schema Update';alembic upgrade head; spacedao_api --host 0.0.0.0 --port 8000;"]
