SpaceDAO API
============

The API for SpaceDAO is meant to organize information and provide analytical
insights on the various decentralized application running on the SpaceDAO
blockchain.

DAO stands for Decentralized Autonomous Organization.
And is meant support Autonomous Orbit Applications and Services.

Space exploration and usage is decentralized by Nature, centralizing it would
only bring global tensions. SpaceDAO is a technological approach with conscious
geo-political concerns.

It kick started thanks to the Open Space Innovation Platform of the European
Space Agency (ESA) as part of the campaign for Cognitive Cloud Computing. The
executive summary of the study is publicly visible on [ESA's projects listing
(called Nebula)](https://nebula.esa.int/content/blockchain-ecosystem-autonomous-consensus-mechanism-federated-satellite-networks-%E2%80%93-space-dao).

The API is an open source artifact under the LGPL-v-3-0 license.

How to use the API
------------------

### Run a local database

Start the database and dev frontend of the database (Adminer) thanks to
docker-compose:
```bash
docker-compose up -d
```
Service started will be:
 - Adminer on http://127.0.0.1:8006
 - Mariadb on 127.0.0.1:3306

The API will connect to these by default.

### Install and run 

Then install and start the API, from a python environment you would have to
prepare yourself.

```bash
pip install .
spacedao_api
```

(coming soon) You can alos use its official deployment at this url: https://api.spacedao.ai


For developpers
---------------

### Manual installation 

#### Install dependencies

All dependencies are listed in `Pipfile` file for pipenv. Install them with:
```bash
git clone https://gitlab.com/parametry-ai/space-dao/backend/spacedao-api.git
cd spacedao-api/
pipenv install
```

(It will create the environment for the folder if this is the first time)

#### Load your environment

All following commands in this README should happen in your newly installed
python environment. To activate it run:
```bash
pipenv shell 
```

### Database migration

The **alembic** tool is used to make creation/migrations of tables. It is parted
of the Pipfile for the developer's environment. It should be installed if you
have run `pipenv install -d` and if you are using the corresponding environment
`pipenv shell`

 - Step 0:
    - adapt secrets/credentials in databse link in `alembic.ini`
    - you have already run spacedao_api at least once in the past.
 - Step 1: `git pull && pip install .` (database models would get installed)
 - Step 2: autogenerate the revisions
 ```bash
    alembic revision --autogenerate -m "Add a meaningful comment here"
 ```
 - Step 3: apply the revisions
 ```bash
    alembic upgrade head
 ```

In the future you only need to repeat step 1 to 3. 
