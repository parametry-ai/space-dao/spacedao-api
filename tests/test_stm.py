"""
Module for testing STM endpoints
"""
from fastapi.testclient import TestClient
from spacedaoapi.main import app as DAOAPP

client = TestClient(DAOAPP)

endpoint_cdm = "/app/stm/v1/cdm/consensus"

def test_get_cdm_bad1():
    response = client.get(endpoint_cdm)
    assert response.status_code == 404
    assert response.json() == {"detail": "Not Found"}


def test_get_cdm_bad2():
    response = client.get(endpoint_cdm+"/")
    assert response.status_code == 404
    assert response.json() == {"detail": "Not Found"}


def test_get_cdm_good():
    response = client.get(endpoint_cdm+"/arbitrary-req-id-test")
    assert response.status_code == 200



# test for exceptions
#    with pytest.raises(FileNotFoundError):
#        _, _ = pldr.read_polaris_data("/tmp/tmp/tmp/a/b/a/b/NOTINSPACE.csv")
