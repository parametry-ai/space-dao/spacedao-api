"""
Module for testing root and basic endpoints 
"""
from fastapi.testclient import TestClient
from spacedaoapi.main import app as DAOAPP



client = TestClient(DAOAPP)


def test_read_main():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "Hello Universe"}

