from pydantic import BaseModel

# Schema for creating a new user
class UserCreate(BaseModel):
    username: str
    email: str
    password: str

# Schema for returning user info
class UserResponse(BaseModel):
    id: int
    username: str
    email: str

    class Config:
        orm_mode = True

# Schema for returning a token
class Token(BaseModel):
    access_token: str
    token_type: str

# Schema for login request
class LoginRequest(BaseModel):
    username: str
    password: str