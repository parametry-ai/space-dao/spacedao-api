from fastapi import FastAPI, Depends, HTTPException, status, Request, Response
from sqlalchemy.ext.asyncio import AsyncSession
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.future import select
from .database import get_db, Base, engine
from .schemas import UserCreate, UserResponse, Token, LoginRequest
from .models import User
from .auth import get_password_hash, authenticate_user, create_access_token, get_current_user
from .config import config
import logging
from dotenv import load_dotenv
from fastapi.middleware.cors import CORSMiddleware

# Load environment variables
load_dotenv()

# Set up logging
logging.basicConfig(level=logging.DEBUG)

# Create FastAPI app with lifespan
async def lifespan(app: FastAPI) -> None:
    async with engine.begin() as conn:
        # Create database tables
        await conn.run_sync(Base.metadata.create_all)
    yield  # Yielding to allow the app to run

app = FastAPI(lifespan=lifespan)

# CORS setup
origins = ["http://localhost:5173"]  # Adjust this to your frontend's origin
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# OAuth2 scheme for password flow
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

@app.get("/")
async def root():
    return {"message": "Welcome to the API", "api_url": config.API_URL}

@app.options("/api/register")
async def options():
    return {"status": "ok"}

# Rate limiting for login attempts
login_attempts = {}

# Registration endpoint
@app.post("/api/register", response_model=UserResponse)
async def register(user: UserCreate, db: AsyncSession = Depends(get_db)):
    try:
        # Check for existing username
        existing_user = await db.execute(
            select(User).where(User.username == user.username)
        )
        if existing_user.scalars().first():
            raise HTTPException(status_code=400, detail="Username already registered.")

        # Check for existing email
        existing_email = await db.execute(
            select(User).where(User.email == user.email)
        )
        if existing_email.scalars().first():
            raise HTTPException(status_code=400, detail="Email already registered.")

        # Create new user
        hashed_password = get_password_hash(user.password)
        new_user = User(username=user.username, email=user.email, password_hash=hashed_password)
        db.add(new_user)
        await db.commit()
        await db.refresh(new_user)
        return new_user

    except Exception as e:
        logging.error(f"Registration error: {str(e)}")
        raise HTTPException(status_code=400, detail=f"Failed to register user: {str(e)}")

# Login endpoint
@app.post("/api/login", response_model=Token)
async def login(login_request: LoginRequest, db: AsyncSession = Depends(get_db)):
    # Rate limiting check
    user_attempts = login_attempts.get(login_request.username, 0)
    if user_attempts >= 5:  # Allow a maximum of 5 attempts
        raise HTTPException(status_code=status.HTTP_429_TOO_MANY_REQUESTS, detail="Too many login attempts. Please try again later.")

    user = await authenticate_user(db, login_request.username, login_request.password)
    if not user:
        login_attempts[login_request.username] = user_attempts + 1
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid credentials")

    # Reset attempts on successful login
    login_attempts[login_request.username] = 0

    access_token = create_access_token(data={"username": user.username})
    return {"access_token": access_token, "token_type": "bearer"}

# Logout endpoint
@app.post("/api/logout")
async def logout(response: Response):
    token = response.headers.get("Authorization")
    if token:
        # Remove "Bearer " prefix
        token = token.split(" ")[1]
        # Blacklist the token
        blacklisted_tokens.add(token)
    return {"detail": "Successfully logged out"}

# User info endpoint
@app.get("/api/user", response_model=UserResponse)
async def get_user_info(current_user: User = Depends(get_current_user)):
    return current_user

# Dependency to check if token is blacklisted
blacklisted_tokens = set()  # Token storage for blacklisting

async def validate_token(token: str):
    if token in blacklisted_tokens:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Token has been invalidated")

# Middleware to check token validity and blacklist
@app.middleware("http")
async def check_blacklisted_tokens(request: Request, call_next):
    token = request.headers.get("Authorization")
    if token:
        token = token.split(" ")[1]  # Remove "Bearer " prefix
        await validate_token(token)
    response = await call_next(request)
    return response
