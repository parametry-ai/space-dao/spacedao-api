import os
from dotenv import load_dotenv

# Load environment variables
load_dotenv()

class Config:
    API_URL = os.getenv("API_URL", "http://localhost:8000/api")  # Default value

config = Config()