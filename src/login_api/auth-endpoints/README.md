
# FastAPI Authentication API

This FastAPI application provides user registration and authentication functionality with JWT support. It utilizes MySQL as the database backend.

## Table of Contents

- [FastAPI Authentication API](#fastapi-authentication-api)
  - [Table of Contents](#table-of-contents)
  - [Features](#features)
  - [Technologies](#technologies)
  - [Setup](#setup)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
  - [Running the Application](#running-the-application)
  - [Endpoints](#endpoints)
    - [Registration](#registration)
    - [Login](#login)
    - [Logout](#logout)
    - [User Info](#user-info)

## Features

- User registration with validation
- User login with JWT generation
- Token-based authentication for accessing protected resources
- Rate limiting for login attempts
- Token blacklisting for logout functionality

## Technologies

- **FastAPI**: A modern web framework for building APIs with Python.
- **SQLAlchemy**: ORM for database interactions.
- **MySQL**: Relational database management system.
- **Uvicorn**: ASGI server for running FastAPI applications.
- **Pydantic**: Data validation and settings management using Python type annotations.
- **Python**: Programming language used for the application.

## Setup

### Prerequisites

- Python 3.8 or later
- MySQL server

### Installation
1. **Create a virtual environment:**

   ```bash
   python -m venv venv
   source venv/bin/activate  # On Windows use `venv\Scripts\activate`
   ```

2. **Install the required packages:**

   ```bash
   pip install -r requirements.txt
   ```

3. **Set up the database:**

   - Create a MySQL database and user (if you haven't done so):

   ```sql
   CREATE DATABASE your_database_name;
   CREATE USER 'your_username'@'localhost' IDENTIFIED BY 'your_password';
   GRANT ALL PRIVILEGES ON your_database_name.* TO 'your_username'@'localhost';
   FLUSH PRIVILEGES;
   ```

4. **Configure environment variables:**

   - Create a `.env` file in the root directory of your project and add the following variables:

   ```env
   DATABASE_URL=mysql+asyncmy://your_username:your_password@localhost/your_database_name
   SECRET_KEY=your_secret_key
   ```

   - Replace `your_username`, `your_password`, `your_database_name`, and `your_secret_key` with your actual values.

## Running the Application

To start the FastAPI application using Uvicorn, run the following command in the user-auth directory:

```bash
uvicorn app.main:app --reload
```

- `main`: The name of your main Python file (without the .py extension).
- `app`: The name of your FastAPI instance.
- `--reload`: Enables auto-reload for development.

The application will be accessible at `http://localhost:8000`.

## Endpoints

### Registration

- **POST** `/api/register`
- **Description**: Registers a new user.
- **Request Body**:

  ```json
  {
    "username": "your_username",
    "email": "your_email@example.com",
    "password": "your_password"
  }
  ```

- **Responses**:
  - **201 Created**: User registered successfully.
  - **400 Bad Request**: If the username or email is already registered.

### Login

- **POST** `/api/login`
- **Description**: Authenticates a user and returns a JWT.
- **Request Body**:

  ```json
  {
    "username": "your_username",
    "password": "your_password"
  }
  ```

- **Responses**:
  - **200 OK**: Returns the JWT token.
  
  ```json
  {
    "access_token": "your_jwt_token",
    "token_type": "bearer"
  }
  ```

  - **401 Unauthorized**: If the credentials are invalid.
  - **429 Too Many Requests**: If the maximum login attempts are exceeded.

### Logout

- **POST** `/api/logout`
- **Description**: Logs out a user by blacklisting the JWT token.
- **Request Headers**:

  ```
  Authorization: Bearer your_jwt_token
  ```

- **Responses**:
  - **200 OK**: Successfully logged out.

### User Info

- **GET** `/api/user`
- **Description**: Retrieves the authenticated user's information.
- **Request Headers**:

  ```
  Authorization: Bearer your_jwt_token
  ```

- **Responses**:
  - **200 OK**: Returns the user information.
  
  ```json
  {
    "id": 1,
    "username": "your_username",
    "email": "your_email@example.com"
  }
  ```

  - **401 Unauthorized**: If the token is invalid or expired.

