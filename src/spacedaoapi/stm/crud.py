from sqlalchemy.orm import Session
from datetime import datetime

from spacedaoapi.stm import models, schemas


def get_accounts(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Account).offset(skip).limit(limit).all()


def get_account(db: Session, address: str):
    return db.query(models.Account).filter(
            models.Account.address == address
        ).first()


def create_account(db: Session, account: schemas.AccountCreate):
    right_now = datetime.utcnow()

    db_account = models.Account(address=account.address,
                                appear_date=right_now,
                                last_updated=right_now)

    db.add(db_account)
    db.commit()
    db.refresh(db_account)

    return db_account


def get_cdms(db: Session, skip: int = 0, limit: int = 100):
    """ Get a list of CDMs """
    return db.query(models.CDM).offset(skip).limit(limit).all()


def get_cdm_by_id(db: Session, cdm_id: str):
    """ Get a CDM by its cdm_id """

    return db.query(models.CDM).filter(
            models.CDM.cdm_id == cdm_id
        ).first()


def get_cdm_by_request_id(db: Session, request_id: str):
    """ Get a CDM by its request_id """

    return db.query(models.CDM).filter(
            models.CDM.request_id == request_id
        ).first()


def create_cdm(db: Session, payload: schemas.CDMCreate):
    """ Create a CDM """

    right_now = datetime.utcnow()
    cdm = payload.cdm

    if not hasattr(payload, 'request_id'):
        raise TypeError("request_id must be set")
    if not hasattr(payload, 'cdm_id'):
        raise TypeError("cdm_id must be set")
    if not hasattr(payload, 'cdm'):
        raise TypeError("cdm must be set")
    # if not hasattr(cdm, 'rso_1'):
    #     raise TypeError("rso_1 must be set")
    # if not hasattr(cdm, 'rso_2'):
    #     raise TypeError("rso_2 must be set")

    db_cdm = models.CDM(request_id=payload.request_id,
                        cdm_id=payload.cdm_id,
                        cdm=payload.cdm,
                        created=right_now,
                        rso_1=cdm['rsos'][0],
                        rso_2=cdm['rsos'][1])

    db.add(db_cdm)
    db.commit()
    db.refresh(db_cdm)

    return db_cdm
