from sqlalchemy import Column, ForeignKey, DateTime
from sqlalchemy import String, JSON  # Integer, Float

# from sqlalchemy.orm import relationship

from spacedaoapi.commons.database import Base, SPACEDAO_TPREFIX

# Table names
table_account = str(SPACEDAO_TPREFIX)+"account"
table_contract = str(SPACEDAO_TPREFIX)+"contract"
table_event = str(SPACEDAO_TPREFIX)+"event"
table_cdm = str(SPACEDAO_TPREFIX)+"cdm"


class Account(Base):
    """ Blockchain account

    Depending on the blockchain used, it can represent:
        - an individual,
        - a contract,
        - an entity,
        - an NFT or SFT, or any kind of tokens.
    """
    __tablename__ = table_account
    address = Column("address", String(64), primary_key=True, index=True)
    # First time there is a visible action on the network for that account
    appear_date = Column("appear_date", DateTime)
    last_updated = Column("last_updated", DateTime)


class Contract(Base):
    """ Blockchain contract

    """
    __tablename__ = table_contract
    address = Column("address", String(64), primary_key=True, index=True)
    owner = Column(String(64), ForeignKey(table_account+".address"))
    # Appearance or creation date
    appear_date = Column("appear_date", DateTime)

    # owner =  relationship("Account", back_populates="otherRelationship")


class Event(Base):
    """ Blockchain event

    """
    __tablename__ = table_event
    address = Column("from_address", String(64), primary_key=True, index=True)
    name = Column("name", String(250))
    # Appearance or creation date
    date = Column("date", DateTime)


class CDM(Base):
    """ Conjunction Data Massage

    """
    __tablename__ = table_cdm
    request_id = Column("request_id", String(64), primary_key=True, index=True)
    cdm = Column("cdm", JSON)
    cdm_id = Column("cdm_id", String(64), index=True)
    created = Column("created", DateTime)
    rso_1 = Column("rso_1", String(64), index=True)
    rso_2 = Column("rso_2", String(64), index=True)
