import json
import httpx
from fastapi import FastAPI
from fastapi.testclient import TestClient
import pytest

# Import your router here
from stm import router

app = FastAPI()
app.include_router(router)

client = TestClient(app)

# Test data
test_cdm = {
  "response": {
    "supplier": {
      "space-track": {
        "name": "space-track",
        "supplier_id": "s_0st1"
      },
      "leolabs": {
        "name": "leolabs",
        "supplier_id": "s_0ll1"
      },
      "privateer": {
        "name": "privateer",
        "supplier_id": "s_0pt1"
      }
    },
    "consensus": 0.719

  }
}

def count_suppliers(cdm):
    if cdm and "response" in cdm and "supplier" in cdm["response"]:
        return len(cdm["response"]["supplier"])
    return 0

@pytest.mark.asyncio
async def test_build_consensus():
    response = client.get("/stm/v1/cdm/consensus", params={"event_id": "test_event_id", "cdm": json.dumps(test_cdm)})
    assert response.status_code == 200
    assert response.json()["consensus_level"] == 0.66 #Updated value
    assert response.json()["supplier_count"] == 3



