from fastapi import APIRouter, Depends
from fastapi.exceptions import HTTPException
from sqlalchemy.orm import Session
# from sse_starlette.sse import EventSourceResponse

from spacedaoapi.stm import crud, schemas
from spacedaoapi.commons.database import SessionLocal


router = APIRouter(
    prefix="/stm/v1",
    tags=["stm"],
    # dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Lost in STM 404 space"}},
)


# ---------------------------- routines
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@router.get("/")
async def version():
    return {"version": "0.5.0"}


# ---------------------------- accounts
@router.get("/accounts", response_model=list[schemas.Account])
async def get_accounts(db: Session = Depends(get_db)):
    acc_list = crud.get_accounts(db)
    return acc_list


@router.get("/accounts/{account_id}", response_model=schemas.Account)
async def get_account_id(account_id: str, db: Session = Depends(get_db)):
    acc = crud.get_account(db, account_id)
    return acc


@router.post("/accounts", response_model=schemas.Account)
async def create_account(account: schemas.AccountCreate,
                         db: Session = Depends(get_db)):
    db_account = crud.get_account(db, address=account.address)
    if db_account:
        raise HTTPException(status_code=400, detail="Account already indexed")
    return crud.create_account(db, account)


# ---------------------------- stats
@router.get("/stats/rso")
async def stats_rso():
    return {"count": 51346, "ttag": "stats.rso.count"}


@router.get("/stats/rso/{rso_id}")
async def stats_rso_id(rso_id: str):
    return {"message": "TODO"}


@router.get("/stats/cdm")
async def stats_cdm():
    return {"count": 412, "ttag": "stats.cdm.count24"}


@router.get("/stats/cdm/{cdm_id}")
async def stats_cdm_id(cdm_id: str):
    return {"message": "TODO"}


@router.get("/stats/suppliers")
async def stats_suppliers():
    return {"count": 32, "ttag": "stats.suppliers.count"}


@router.get("/stats/supplier/{supplier_id}")
async def stats_supplier_id(supplier_id: str):
    return {"message": "TODO"}


# ---------------------------- cdms
@router.get("/cdm/consensus/{request_id}")
async def get_consensus(request_id: str):
    """ Get **consensus** about a **request_id**

        Users or their scheduling agent emit a request for a CDM consensus.
        When the consensus is ready on the network it is broadcasted for other
        automated applications (other smart contracts) to process.

        A **request_id** is the on-chain ID of the initial request made by
        users/requesters.

    """
    # Get the data from database return 404 if not found
    if (len(request_id) < 8):
        # this can't be a valid ID
        raise HTTPException(status_code=404, detail="requested ID not found")

    # HERE GET CONSENSUS from database TODO
    # get consensus
    # get metadata for each data supplier and satellites
    cdm_data = {
        "event_id": "event_id",
        "created": "DATE PLACEHOLDER",
        "pc": 0.5,
        "distance_m": 412,
        "tca": "DATE PLACEHOLDER",
        "rso_list": [{"name": "STARLINK 222"}, {"name": "COSMOS R341"}],
        "sources": [
                {"name": "P1", "id": "P1_id",
                 "confidence_model": 0.8,
                 "confidence_direction": "up",
                 "p_distance": 0.02},
                {"name": "P2", "id": "P2_id",
                 "confidence_model": 0.75,
                 "confidence_direction": "up",
                 "p_distance": 0.01},
                {"name": "P3", "id": "P3_id",
                 "confidence_model": 0.82,
                 "confidence_direction": "down",
                 "p_distance": 0.4}
            ],
        "consensus_level": 0.5

        # what information is shown if the consensus was rerun because too low.
        # if consensus_level < user_accepted_consensus_level |
        #                      default_accepted_consensus_level
        # then recompute with more additional sources
    }

    # HERE DO THE POST-PROCESSING
    # e.g., to calculate the consensus level and supplier count
    consensus_level = 0.66  # TODO TO BE CALCULATED
    supplier_count = len(cdm_data["sources"])
    cdm_data["consensus_level"] = consensus_level
    cdm_data["supplier_count"] = supplier_count

    return cdm_data


@router.post("/cdm/consensus", response_model=schemas.CDM)
async def create_cdm_consensus(cdm_payload: schemas.CDMCreate,
                               db: Session = Depends(get_db)):

    db_cdm = crud.get_cdm_by_request_id(db, request_id=cdm_payload.request_id)
    if db_cdm:
        raise HTTPException(status_code=400, detail="CDM already indexed")
    return crud.create_cdm(db, cdm_payload)


@router.get("/cdm", response_model=list[schemas.CDM])
async def get_cdms(db: Session = Depends(get_db)):
    cdm_list = crud.get_cdms(db)
    return cdm_list


@router.get("/cdm/{cdm_id}", response_model=schemas.CDM)
async def get_cdm_id(cdm_id: str, db: Session = Depends(get_db)):
    cdms = crud.get_cdm_by_id(db, cdm_id)
    return cdms


@router.get("/cdm/request/{request_id}", response_model=schemas.CDM)
async def get_cdm_request_id(request_id: str, db: Session = Depends(get_db)):
    cdms = crud.get_cdm_by_request_id(db, request_id)
    return cdms
