from pydantic import BaseModel
import datetime


class AccountBase(BaseModel):
    appear_date: datetime.datetime | None = None
    last_updated: datetime.datetime | None = None


class AccountCreate(AccountBase):
    address: str


class Account(AccountBase):
    address: str

    class Config:
        orm_mode = True


class CDMBase(BaseModel):
    cdm: dict
    cdm_id: str


class CDMCreate(CDMBase):
    request_id: str
    # rso_1: str | None = None
    # rso_2: str | None = None


class CDM(CDMBase):
    request_id: str
    created: datetime.datetime | None = None
    rso_1: str | None = None
    rso_2: str | None = None

    class Config:
        orm_mode = True
