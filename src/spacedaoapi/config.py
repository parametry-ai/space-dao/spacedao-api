""" Default Configuration

 To be overridden by command line options.
"""

# ------------ DEFAULT SETUP
MYSQL_USER = "yvonne"
MYSQL_PASSWORD = "cagle"
DATABASE_HOST = "127.0.0.1"
DATABASE_PORT = 3306
DATABASE_NAME = "spacedao"
production = True

# For local test only define ports of your frontend
origins_testing_ports = [8100, 8080, 8000]

# For production indicate all domains allowed to interact with your API
origins_production = [
    "http://api.spacedao.ai",
    "http://app.spacedao.ai",
    "http://spacedao.ai",
    "http://www.spacedao.ai"
]

# ------------ END OF CUSTOM SETUP
origins_testing = ["http://localhost", "http://127.0.0.1"]
for port in origins_testing_ports:
    origins_testing.append("http://localhost:"+str(port))
    origins_testing.append("http://127.0.0.1:"+str(port))

origins = origins_testing
if production is True:
    origins = origins_production
