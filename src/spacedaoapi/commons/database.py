import os
import sys
import socket
from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import sessionmaker
from spacedaoapi.config import DATABASE_NAME, DATABASE_HOST, DATABASE_PORT
import pymysql


# --- Loading credentials
cwd = os.getcwd()
print(cwd)
# importing following variables:
#   MYSQL_USER
#   MYSQL_PASSWORD
#   MYSQL_ROOT_PASSWORD
#   DATABASE_*
if os.path.isfile(cwd+"/credentials.py"):
    sys.path.append(cwd)
    from credentials import *
else:
    if os.path.isfile(cwd+"/credentials_sample.py"):
        sys.path.append(cwd)
        from credentials_sample import *
    else:
        raise ImportError(
            "No credentials file could be imported: ./credentials.py")


# --- Check if host is available else replace by localhost
try:
    socket.gethostbyname(DATABASE_HOST)
except socket.error:
    DATABASE_HOST = "localhost"

# --- root connect to DB
myrootdb = pymysql.connect(
  host=DATABASE_HOST,
  port=DATABASE_PORT,
  user="root",
  password=MYSQL_ROOT_PASSWORD
)

rootdbcursor = myrootdb.cursor()
rootdbcursor.execute("SHOW DATABASES")

# --- check and create DB and permissions
db_list = [x[0] for x in rootdbcursor]
if DATABASE_NAME in db_list:
    print(f"[ok] Database {DATABASE_NAME} already exists.")
    print(f"\tAssuming correct permissions are granted on {DATABASE_NAME}.*")
else:
    print(f"[act] Creating {DATABASE_NAME} database for user {MYSQL_USER}")
    rootdbcursor.execute(f"CREATE DATABASE {DATABASE_NAME};")
    if len(MYSQL_USER) > 3:
        print(f"[act] Permissions on {DATABASE_NAME}.* for user {MYSQL_USER}")
        # Removed option:  + "WITH GRANT OPTION;")
        rootdbcursor.execute("GRANT CREATE, ALTER, DROP, INSERT, UPDATE, "
                             + "INDEX, DELETE, SELECT, REFERENCES "
                             + f"ON {DATABASE_NAME}.* "
                             + f"TO '{MYSQL_USER}'@'%' ;")
        # Global privilege only possible on *.*
        rootdbcursor.execute("GRANT RELOAD ON *.* "
                             + f"TO '{MYSQL_USER}'@'%' ;")

# --- Setup SQLAlchemy for this project
SQLALCHEMY_DATABASE_URL = ""
# SQLALCHEMY_DATABASE_URL = "sqlite:///./sql_app.db"
# SQLALCHEMY_DATABASE_URL = "postgresql://user:password@postgresserver/db"
if isinstance(MYSQL_USER, str) and isinstance(MYSQL_PASSWORD, str) \
   and isinstance(DATABASE_HOST, str):
    SQLALCHEMY_DATABASE_URL = "mysql+pymysql://{}:{}@{}:{}/{}".format(
        MYSQL_USER,
        MYSQL_PASSWORD,
        DATABASE_HOST,
        DATABASE_PORT,
        DATABASE_NAME)
else:
    print("Database credentials are not strings.")

print("Space DAO API info: Database URL = {}".format(SQLALCHEMY_DATABASE_URL))

engine = create_engine(
    # SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
    SQLALCHEMY_DATABASE_URL
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# ------------------- Most used in imports
Base = declarative_base()
Base.metadata.create_all(bind=engine)

SPACEDAO_TPREFIX = "sdao_"
EXTERNAL_TPREFIX = "ext_"
