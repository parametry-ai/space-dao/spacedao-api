import uvicorn
from fastapi import FastAPI  # APIRouter, Depends, HTTPException
from fastapi.middleware.cors import CORSMiddleware
import typer
from typing_extensions import Annotated
from typing import Optional
from spacedaoapi.config import origins
from spacedaoapi.stm import stm

description = """
SpaceDAO API helps you retrieve and update related DAO DApp information.
It acts like an aggregator and relay to the Space DAO distributed network.

## More info on spacedao.ai

"""

cmdline = typer.Typer()

app = FastAPI(
    title="Space DAO API",
    description=description,
    version="0.1.0",
    terms_of_service="https://spacedao.ai/terms/",
    contact={
        "name": "Space DAO contributors",
        "url": "https://spacedao.ai"
    },
    license_info={
        "name": "LGPL-3.0",
        "url": "https://opensource.org/licenses/LGPL-3.0"
    }
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["OPTIONS", "POST", "PUT", "GET"],
    allow_headers=["*"],
    expose_headers=["Access-Control-Allow-Origin"]
)


# ------------- routers
PREFIX_APP = "/app"
app.include_router(stm.router, prefix=PREFIX_APP)


# ------------- root calls
@app.get("/")
async def root():
    return {"message": "Hello Universe."}


# ------------- main command line
@cmdline.command()
def run(host: Annotated[Optional[str], typer.Option(
            help="Host IP address to bind to.")] = "127.0.0.1",
        port: Annotated[Optional[int], typer.Option(
            help="Port number to use to serve the API.")] = 8000,
        workers: Annotated[Optional[int], typer.Option(
            help="Number of workers for FastAPI engine.")] = 2
        ):
    # use config content here
    uvicorn.run("spacedaoapi.main:app", host=host, port=port, workers=workers)
