#!/bin/sh

# --- preparing build env
echo " ----- preparing build environment"
mkdir ./build

# pipenv requirements > ./build/requirements.txt
tox -e build
ls -rt1 dist/*.whl | tail -n1 > ./build/pkg_name

echo " ----- building image"
tag=$(git tag -l | tail -n 1)

docker build -t spacedao-api:${tag} .

echo " ----- saving image"
docker save spacedao-api:${tag} > build/spacedao-api--${tag}.tar.gz

echo "   [ok]   $(ls -rtl build/ | tail -n1) "
echo ""
echo " ----- You can now run: "
echo "docker run -d --name spacedao-api-local -p 127.0.0.1:8000:8000 spacedao-api:${tag}"

